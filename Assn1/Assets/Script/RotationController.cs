﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class RotationController : MonoBehaviour
{
    public float SENS_HOR = 3.0F;
    public float SENS_VER = 2.0F;
	public bool isInverted;
	private float minX = 285;
    private float maxX = 78;
    GameObject character;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
		isInverted = false;
        character = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        var mouseMove = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        mouseMove = Vector2.Scale(mouseMove, new Vector2(SENS_HOR, SENS_VER));

        character.transform.Rotate(0, mouseMove.x, 0);
		if(isInverted == false)
		{
			transform.Rotate(-mouseMove.y, 0, 0);
		}else
		{
			transform.Rotate(mouseMove.y, 0, 0);
		}
		Vector3 currentRotation = transform.localRotation.eulerAngles; // gets the current angle(only works for up and down which is the X axis, 
                                                                       // can't seem to get y or z to have any values unless it starts tilting sideways which shouldn't be happening)
                                                                       // debugging code
                                                                       //if (Input.GetKeyDown(KeyCode.I))                             
                                                                       //{
                                                                       //    //Debug.Log("X rot:" + currentRotation.x + " Y rot:" + currentRotation.y + " Z rot:" + currentRotation.z);
                                                                       //    Debug.Log("X rot:" + transform.localRotation.eulerAngles.x + " Y rot:" + transform.localRotation.eulerAngles.y);
                                                                       //}
                                                                       //if (Input.GetKeyDown(KeyCode.K))
                                                                       //{
                                                                       //    Debug.Log("X pos:" + transform.position.x + " Y pos:" + transform.position.y + " Z pos:" + transform.position.z);
                                                                       //}                                                            // end of debug print code

        if (currentRotation.x >= maxX && currentRotation.x <= 180) //this is the lower half of the look angle
        {
            currentRotation.x = Mathf.Clamp(currentRotation.x, 0, 78);//0 is the middle of the screen, 89 is the bottom most, feel free to adjust based on what you need
            currentRotation.y = 0;                                    //lower value = tighter look radius at the bottom, higher value = higher look radius
            currentRotation.z = 0;                                    //I froze the other rotations since they only increment/decrement when the angle goes weird(sideways tilt, etc.)
            transform.localRotation = Quaternion.Euler(currentRotation);
        }
        else if (currentRotation.x > 180 && currentRotation.x <= minX)//upper half of the look angle
        {
            currentRotation.x = Mathf.Clamp(currentRotation.x, 285, 360);//same idea as above(respectively, top, middle of the screen)
            currentRotation.y = 0;                                       //<285 means wider look angle, >285 means tighter look angle
            currentRotation.z = 0;                                       //once again, freeze the y and z rotation angles.
            transform.localRotation = Quaternion.Euler(currentRotation);
        }
        else
        {
            currentRotation.y = 0;
            currentRotation.z = 0;
            transform.localRotation = Quaternion.Euler(currentRotation);
        }
        //vertical
        currentRotation.y = 0;
        currentRotation.z = 0;
        if (Input.GetKeyDown("escape"))
            Cursor.lockState = CursorLockMode.None;
    }
}
