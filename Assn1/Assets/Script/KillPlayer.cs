﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour
{
    public GameObject door;
    public Transform respawnPoint;
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")  // check the tag
        {
            col.gameObject.transform.position = respawnPoint.transform.position;
            door.GetComponent<Animator>().SetTrigger("Open");
        }
    }
}
