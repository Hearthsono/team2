﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float impactForce = 30f;
    public float fireRate = 15f;

    private float nextFire = 0f;

    public Camera fpsCam;
    //public ParticleSystem muzzleFlash;
    //public GameObject impactEffect; 

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0) && Time.time > nextFire) {
            nextFire = Time.time + 1f / fireRate;
            Shoot();
        }
    }

    public void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.DrawRay(fpsCam.transform.position, fpsCam.transform.forward, Color.red, 2f);
            Debug.Log(hit.transform.name);
            EnemyTarget hitTarget = hit.transform.GetComponent<EnemyTarget>();
            if (hitTarget != null) {
                hitTarget.takeDmg(damage);
            }
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }

            //GameObject ImpEff = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            //Destroy(ImpEff, 1f);
        }

    }
}
