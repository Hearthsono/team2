﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnMove : MonoBehaviour
{
    Animator colAnim;
    GameObject[] Enemy;
    // Start is called before the first frame update
    void Start()
    {
        colAnim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        Enemy = GameObject.FindGameObjectsWithTag("Enemy2");

        if (Enemy.Length == 0)
        {
            colAnim.SetTrigger("Up");
        }
    }
}
