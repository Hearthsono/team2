﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class EnemyTarget : MonoBehaviour
{
    public float health = 50f;
    public GameObject Win;
    public GameObject UI;
    private Animator anim;

    public void Start()
    {
        anim = GetComponentInChildren<Animator>();
        //Win = GameObject.FindGameObjectWithTag("Win");
        //Debug.Log(Win);
        if (gameObject.tag == "Boss")
        {
            Win.SetActive(false);
        }
        //UI = GameObject.FindGameObjectWithTag("Finish");
    }

    public void takeDmg(float amt) {
        health -= amt;
        //Debug.Log(health);
        if (health <= 0f) {
            if (gameObject.tag == "Boss")
            {
                Win.SetActive(true);
                UI.SetActive(false);
            }
            anim.SetTrigger("Death");
            StartCoroutine(Die());
        }
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(4f);
        Destroy(gameObject);
    }
}
