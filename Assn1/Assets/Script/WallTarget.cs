﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WallTarget : MonoBehaviour
{
    public float health = 1f;

    public void takeDmg(float amt)
    {
        health -= amt;
        Debug.Log(health);
        if (health <= 0f)
        {
            Destroy(gameObject);
        }
    }


}
