﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pistol : MonoBehaviour
{
    //bullet count stuff
    public Text BulletText;
    public int MaxBullet = 13;
    private int currentBullet = 13;

    public float damage = 8f;
    public float range = 100f;
    public float impactForce = 5f;

    public Camera fpsCam;
    Animator anim;

    public AudioSource draw;
    public AudioSource fire;
    public AudioSource reload;

    // Start is called before the first frame update
    void Start()
    {
        draw.Play();
        anim = GetComponent<Animator>();
        anim.SetBool("Idle", true);

        currentBullet = MaxBullet;
        BulletText.text = "Bullets: " + currentBullet;
    }

    // Update is called once per frame
    void Update()
    { 
        if (anim.GetBool("Idle"))
        {   // Fire
            if (Input.GetMouseButtonDown(0))
            {
                if (currentBullet <= 0)
                {
                    return;
                }
                anim.SetBool("Fire", true);
                anim.SetBool("Idle", false);
                currentBullet--;
                anim.speed = 6;
                StartCoroutine(WaitForFire());
                Shoot();
            }

            // Reload
            if (Input.GetKeyDown(KeyCode.R) && (currentBullet < MaxBullet))
            {
                anim.SetBool("Reload", true);
                anim.SetBool("Idle", false);
                StartCoroutine(WaitForReload());
            }
        } 
        
        // Swapping Weapons
        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Alpha3)) // Swapping to Shotgun or Rifle respectively
        {
            anim.SetTrigger("Holster");
        }

        BulletText.text = "Bullets: " + currentBullet;
    }

    void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.DrawRay(fpsCam.transform.position, fpsCam.transform.forward, Color.red, 2f);
            Debug.Log(hit.transform.name);
            EnemyTarget hitTarget = hit.transform.GetComponent<EnemyTarget>();
            if (hitTarget != null)
            {
                hitTarget.takeDmg(damage);
            }
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            WallTarget wt = hit.transform.GetComponent<WallTarget>();
            if (wt != null)
            {
                wt.takeDmg(damage);
            }
            //GameObject ImpEff = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            //Destroy(ImpEff, 1f);
        }

    }
    IEnumerator WaitForReload()
    {
        reload.Play();
        yield return new WaitForSeconds(1.083f);
        anim.SetBool("Reload", false);
        anim.SetBool("Idle", true);
        currentBullet = MaxBullet;
    }
    IEnumerator WaitForFire() // Delay between firing
    {
        fire.Play();
        yield return new WaitForSeconds(0.25f);
        anim.SetBool("Fire", false);
        anim.SetBool("Idle", true);
        anim.speed = 1;
    }
}
