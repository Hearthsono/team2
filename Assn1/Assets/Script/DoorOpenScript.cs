﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenScript : MonoBehaviour
{
    Animator doorAnim;
    bool doorOpen;
    GameObject[] Enemy;
    // Start is called before the first frame update
    void Start()
    {
        doorOpen = false;
        doorAnim = GetComponent<Animator>();
      
    }

    // Update is called once per frame
    void Update()
    {
        Enemy = GameObject.FindGameObjectsWithTag("StartingEnemy");
        if (Enemy.Length == 0)
        {
            doorOpen = true;
            doorAnim.SetTrigger("Open");
        }
        else if (Enemy.Length >0)
        {
            if (doorOpen == true)
            {
                doorAnim.ResetTrigger("Open");
                doorOpen = false;
                doorAnim.SetTrigger("Close");
            }
        }
    }
}
