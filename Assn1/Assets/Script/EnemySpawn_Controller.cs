﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn_Controller : MonoBehaviour
{
    Collider col;
    public Vector3 spawnPos;
    public List<GameObject> enemyList;
    float timer;
    bool spawning;

    void Start()
    {
        col = GetComponent<Collider>();
        timer = Time.time;
        spawning = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1)) {
            //Instantiate(enemyTest, spawnPos, Quaternion.identity );
        }
        if (spawning)
        {
            if (enemyList.Count > 0)
            {
                if (Time.time - timer > 3f)
                {
                    Instantiate(enemyList[0], new Vector3(spawnPos.x, spawnPos.y, (spawnPos.z--)), Quaternion.identity);
                    enemyList.RemoveAt(0);
                    timer = Time.time;
                }
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Debug.Log("player collided");
            spawning = true;
            col.enabled = false;
        }
        
    }
}
