﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shotgun : MonoBehaviour
{
    //bullet count stuff
    public Text BulletText;
    public int MaxBullet = 6;
    private int currentBullet = 6;

    public float damage = 20f;
    public float range = 7f;
    public float impactForce = 30f;

    public Camera fpsCam;
    Animator anim;

    public AudioSource draw;
    public AudioSource fire;
    public AudioSource reload;

    // Start is called before the first frame update
    void Start()
    {
        draw.Play();
        anim = GetComponent<Animator>();
        anim.SetBool("Idle", true);
        Debug.Log(anim.GetBool("Idle"));
        currentBullet = MaxBullet;
        BulletText.text = "Bullets: " + currentBullet;
    }

    // Update is called once per frame
    void Update()
    { 
        if (anim.GetBool("Idle"))
        {   // Fire
            if (Input.GetMouseButtonDown(0))
            {
                if (currentBullet <= 0)
                {
                    return;
                }
                anim.SetBool("Fire", true);
                anim.SetBool("Idle", false);
                currentBullet--;
                StartCoroutine(WaitForFire());
                anim.speed = 6;
                fire.Play();
                Shoot();
            }
            
            // Reload
            if (Input.GetKeyDown(KeyCode.R) && (currentBullet < MaxBullet))
            {
                StartCoroutine(WaitForReloadStart());
            }
        }
        
        // Swapping Weapons
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Alpha3)) // Swapping to Pistol or Rifle respectively
        {
            anim.SetTrigger("Holster");
        }

        BulletText.text = "Bullets: " + currentBullet;
    }

    void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.DrawRay(fpsCam.transform.position, fpsCam.transform.forward, Color.red, 2f);
            Debug.Log(hit.transform.name);
            EnemyTarget hitTarget = hit.transform.GetComponent<EnemyTarget>();
            if (hitTarget != null)
            {
                hitTarget.takeDmg(damage);
            }
            if (hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            WallTarget wt = hit.transform.GetComponent<WallTarget>();
            if (wt != null)
            {
                wt.takeDmg(damage);
            }
            //GameObject ImpEff = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            //Destroy(ImpEff, 1f);
        }

    }
    IEnumerator WaitForReloadStart()
    {
        yield return new WaitForSeconds(0.41666f);
        anim.SetBool("Reload", true);
        anim.SetBool("Idle", false);
        StartCoroutine(WaitForReloadLoop());
    }
    IEnumerator WaitForReloadLoop()
    {
        while (currentBullet < MaxBullet)
        {
            yield return new WaitForSeconds(0.5f);
            reload.Play();
            currentBullet++;
        }
        anim.SetTrigger("Reload_End");
        StartCoroutine(WaitForReloadEnd());
    }
    IEnumerator WaitForReloadEnd()
    {
        yield return new WaitForSeconds(0.41666f);
        anim.SetBool("Reload", false);
        anim.SetBool("Idle", true);
    }
    IEnumerator WaitForFire() // Delay between firing
    {
        yield return new WaitForSeconds(0.583f);
        anim.SetBool("Fire", false);
        anim.SetBool("Idle", true);
        anim.speed = 1;
    }
}