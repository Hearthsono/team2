﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public GameObject player;

    public Image Imghealth;

    public Text TxtHealth;

    public int min;

    public int max;

    private float mCurrentValue;

    private float mCurrentPercent;

    public void SetHealth (float health)
    {
        if (health != mCurrentValue)
        {
            if (max - min == 0)
            {
                mCurrentValue = 0;
                mCurrentPercent = 0;
            }
            else
            {
                mCurrentValue = health;

                mCurrentPercent = (float)mCurrentValue / (float)(max - min);
            }

            TxtHealth.text = string.Format("{0} %", Mathf.RoundToInt(mCurrentPercent * 100));
            Imghealth.fillAmount = mCurrentPercent;
        }
    }

    public float CurrentPercent
    {
        get { return mCurrentPercent; }
    }

    public float CurrentValue
    {
        get { return mCurrentValue; }
    }
    // Start is called before the first frame update
    void Start()
    {
        //SetHealth(50);
        player = GameObject.Find("Player");
        SetHealth(player.GetComponent<PlayerStats>().player_HP);
    }

    void Update()
    {
        SetHealth(player.GetComponent<PlayerStats>().player_HP);
    }
}
