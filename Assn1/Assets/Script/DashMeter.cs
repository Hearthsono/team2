﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashMeter : MonoBehaviour
{
    public GameObject player;

    public Image Imgdash;

    private float mCurrentPercent;

    private float dashCD;

    private float dashTime;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        dashCD = player.GetComponent<PlayerStats>().DashCD;
        dashTime = dashCD;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift) && dashTime >= dashCD)
        {
            Imgdash.fillAmount = 0.0f;
            dashTime = 0.0f;
        }
        dashTime += Time.deltaTime;
        mCurrentPercent = dashTime / dashCD;
        Imgdash.fillAmount = mCurrentPercent;
    }
}
