﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorClose : MonoBehaviour
{
    Animator doorAnim;
    public GameObject boss;
    // Start is called before the first frame update
    void Start()
    {
        doorAnim = GetComponent<Animator>();
        boss.SetActive(false);
    }

    // Update is called once per frame
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")  // check the tag
        {
            doorAnim.SetTrigger("Close");
            doorAnim.ResetTrigger("Open");
            boss.SetActive(true);
            // Debug.Log("Player Entered Trigger Zone");      
        }
    }
}
