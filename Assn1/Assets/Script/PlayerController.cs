﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float DASH_COOLDOWN = 3.0F;
    private float SPEED = 10.0f;
    private float jumpForce = 3.0f;
    private Rigidbody rb;
    private Vector3 jump;
    private bool isGrounded;
    private float NextDash;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 2.0f, 0.0f);
    }
    //activates when character stays on a collision space i.e. the ground
    void OnCollisionStay()
    {
        isGrounded = true;
    }
    // Update is called once per frame
    void Update()
    {
        
        float mvX = Input.GetAxis("Horizontal") * Time.deltaTime * SPEED;
        float mvZ = Input.GetAxis("Vertical") * Time.deltaTime * SPEED;

        transform.Translate(mvX, 0, mvZ);

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {

            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && Time.time > NextDash)//dash
        {
            NextDash = Time.time + DASH_COOLDOWN;
            rb.AddForce(transform.forward * 10.0f, ForceMode.Impulse);            
        }
       
    }

}
