﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public float player_HP;
    public bool[] weapons = { true, false, false };
    //public bool[] weapons = { true, true, true };
    public GameObject[] equiped;
    public float DashCD = 0f;
    public Transform ResPoint;
    // Start is called before the first frame update
    void Start()
    {
        player_HP = 100;
        equiped[0].SetActive(true);
        equiped[1].SetActive(false);
        equiped[2].SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (player_HP <= 0)
        {
            this.transform.position = ResPoint.transform.position;
            player_HP = 100;
        }
        if (Input.GetKeyDown(KeyCode.Alpha1) && weapons[0] == true)
        {
            equiped[0].SetActive(true);
            equiped[1].SetActive(false);
            equiped[2].SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && weapons[1] == true)
        {
            equiped[0].SetActive(false);
            equiped[1].SetActive(true);
            equiped[2].SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && weapons[2] == true)
        {
            equiped[0].SetActive(false);
            equiped[1].SetActive(false);
            equiped[2].SetActive(true);
        }
        Debug.Log(weapons[1]);
    }

    //set collsion stuff here
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Enemy")
        {

        }
    }

    public void takeDmg(float amt)
    {
        player_HP -= amt;
        //Debug.Log(player_HP);
        if (player_HP <= 0f)
        {
            //Win.SetActive(true);
            //UI.SetActive(false);
            //Destroy(gameObject);
        }
    }
}
