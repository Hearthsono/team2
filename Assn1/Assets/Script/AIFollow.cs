﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIFollow : MonoBehaviour
{
    public NavMeshAgent agent;
    GameObject player;

    public Transform[] waypoints;
    private int currentPoint = 0;
    private Vector3 target;
    private Vector3 direction;

    public float fireRate = 1f;
    private float nextFire = 0f;
    public float attackRange;
    public AudioSource fire;
    public float limit;

    private Animator anim;
    private string botState = "Idle";

    public float allowedDistance;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        allowedDistance = GetComponent<NavMeshAgent>().stoppingDistance;
        anim = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        EnemyTarget EH = GetComponent<EnemyTarget>();
        //if (Input.GetMouseButtonDown(0))
        //{
        //    transform.LookAt(player.transform);
        //    agent.SetDestination(player.transform.position);
        //}
        //transform.LookAt(player.transform);
        //agent.SetDestination(player.transform.position);
        //GetComponent<NavMeshAgent>().stoppingDistance = allowedDistance;

        Vector3 direction = player.transform.position - transform.position;
        //Debug.Log(direction.magnitude);
        //float angleOfView = Vector3.Angle(direction, this.transform.forward);
        //if (direction.magnitude < 10 && angleOfView < 40)
        if (EH.health > 0)
        {
            if (direction.magnitude < limit)
            {
                direction.y = 0;
                //this.transform.rotation = Quaternion.LookRotation(direction);
                transform.LookAt(player.transform);
                agent.SetDestination(transform.position);
                if (direction.magnitude <= attackRange)
                {
                    //Debug.Log("shooting");
                    if (Time.time > nextFire)
                    {
                        nextFire = Time.time + 3f;
                        if (gameObject.tag != "Boss")
                        {
                            EnemyShoot ES = GetComponent<EnemyShoot>();
                            ES.Shoot();
                            StartCoroutine(GunSound());
                        }
                        anim.SetTrigger("Attack");
                        anim.SetBool("Move", false);
                        anim.SetBool("Idle", true);
                    }
                    //this.transform.Translate(0, 0, 0.04F);
                    botState = "Attack";
                }
                
            }
            else
            {
                agent.SetDestination(player.transform.position);
                anim.SetBool("Move", true);
                anim.SetBool("Idle", false);
                //botState = "Move";
            }
            if (botState != "Attack")
            {
                if (waypoints != null)
                {
                    if (gameObject.tag != "Boss")
                    {
                        if (waypoints.Length > 0)
                        {
                            target = waypoints[currentPoint].position;
                            direction = target - transform.position;
                            //Debug.Log(direction.magnitude);
                            if (direction.magnitude < 1)
                            {
                                if (currentPoint < waypoints.Length - 1)
                                {
                                    currentPoint++;
                                }
                                else
                                {
                                    currentPoint = 0;
                                }
                                //transform.LookAt(waypoints[currentPoint].position);
                                //agent.SetDestination(waypoints[currentPoint].position);
                            }
                            anim.SetTrigger("Move");
                            agent.SetDestination(waypoints[currentPoint].position);
                        }
                    }
                }
            }           
        }
        else
        {
            agent.SetDestination(this.transform.position);
        }
    }

    public string getState()
    {
        return botState;
    }

    IEnumerator GunSound()
    {
        yield return new WaitForSeconds(0.8f);
        fire.Play();
    }
}
